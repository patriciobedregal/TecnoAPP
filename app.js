const morgan = require('morgan');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();


//middleware
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));

//routes



app.listen(3000, () => {
    console.log('server on port' , 3000);
});
